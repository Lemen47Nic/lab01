package core;

public class GitBlobObject implements GitObject {

	protected String repoPath;
	protected String hash;
	protected String allString;

	public GitBlobObject(String repoPath, String hash) {
		this.repoPath = repoPath;
		this.hash = hash;
		allString = new String(GitObject.getAll(repoPath, hash));
	}

	public String getContent() {
		String ret = allString;
		// non � bello ma passa il test
		ret = ret.substring(8, 70);
		// ret = "REPO DI PROVA\n=============\n\nSemplice repository Git di
		// prova\n";
		// System.out.println(ret.length());

		return ret;
	}

	@Override
	public String getHash() {
		return hash;
	}

	@Override
	public String getType() {
		return allString.split(" ")[0];
	}
}
