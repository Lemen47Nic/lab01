package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class GitRepository {

	private String repoPath;

	GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}

	public String getHeadRef() {
		String result = "";
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(repoPath + "/HEAD"));
			result = buffer.readLine().substring(5);// taglia 'ref: '
			buffer.close();
		} catch (IOException e) {
			e.printStackTrace();
			result = "file not found";
		}

		return result;
	}

	public String getRefHash(String pathMaster) throws NoSuchAlgorithmException {
		String result = "";
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(repoPath + "/" + pathMaster));
			result = buffer.readLine();
			buffer.close();
		} catch (IOException e) {
			e.printStackTrace();
			result = "file not found";
		}

		return result;
	}

}