package core;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;

public interface GitObject {

	public static byte[] getAll(String repoPath, String hash) {
		byte[] buf = new byte[500];
		String folder = hash.substring(0, 2);
		String file = hash.substring(2);
		try {
			InputStream in = new InflaterInputStream(new FileInputStream(repoPath + "/objects/" + folder + "/" + file));
			in.read(buf);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return buf;
	}

	public String getHash();

	public String getType();
}
