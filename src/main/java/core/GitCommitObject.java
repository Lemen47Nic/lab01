package core;

public class GitCommitObject implements GitObject {

	protected String repoPath;
	protected String hash;
	protected String allString;

	public GitCommitObject(String repoPath, String hash) {
		this.repoPath = repoPath;
		this.hash = hash;
		allString = new String(GitObject.getAll(repoPath, hash));
	}

	public String getTreeHash() {
		return allString.substring(16, 56);
	}

	public String getParentHash() {
		return allString.substring(64, 104);
	}

	public String getAuthor() {
		return allString.substring(112, 156);
	}

	@Override
	public String getHash() {
		return hash;
	}

	@Override
	public String getType() {
		return allString.split(" ")[0];
	}

}
