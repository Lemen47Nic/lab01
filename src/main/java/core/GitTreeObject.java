package core;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import javax.xml.bind.DatatypeConverter;

public class GitTreeObject implements GitObject {

	private String repoPath;
	private String masterTreeHash;
	private String allString;
	private byte[] allBytes;

	public GitTreeObject(String repoPath, String masterTreeHash) {
		this.repoPath = repoPath;
		this.masterTreeHash = masterTreeHash;
		allBytes = GitObject.getAll(repoPath, masterTreeHash);
		allString = new String(allBytes);
	}

	public String[] getEntryPaths() {
		String s = allString.replaceAll("\\P{Print}.*? ", " ");
		// System.out.println(s);
		s = s.replaceAll("\\P{Print}.*", " ");
		// System.out.println(s);
		s = s.substring(9);
		String[] ss = s.split(" ");
		// for (int i = 0; i < ss.length; i++) {
		// System.out.println(ss[i]);
		// }
		return ss;
	}

	public GitObject getEntry(String searchString) {
		int startHash = allString.indexOf(searchString) + searchString.length() + 1;
		// gli hash sono lunghi 20 in git
		byte[] bytes = Arrays.copyOfRange(allBytes, startHash, startHash + 20);
		String file = DatatypeConverter.printHexBinary(bytes).toLowerCase();
		String check = new String(GitObject.getAll(repoPath, file)).split(" ")[0];
		switch (check) {
		case "blob":
			return new GitBlobObject(repoPath, file);
		case "tree":
			return new GitTreeObject(repoPath, file);
		case "commit":
			return new GitCommitObject(repoPath, file);
		default:
			return null;
		}
	}

	@Override
	public String getHash() {
		return masterTreeHash;
	}

	@Override
	public String getType() {
		return allString.split(" ")[0];
	}

}
